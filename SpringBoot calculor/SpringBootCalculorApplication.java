package com.example.springbootcalculor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCalculorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCalculorApplication.class, args);
    }

}
