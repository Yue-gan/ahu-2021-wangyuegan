package com.example.springbootcalculor;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculorController {

    @RequestMapping("/add")
    public double add(@RequestParam("a") double a, @RequestParam("b") double b) {
        return a + b;
    }

    @RequestMapping("/sub")
    public double sub(@RequestParam("a") double a, @RequestParam("b") double b) {
        return a - b;
    }

    @RequestMapping("/multiply")
    public double multiply(@RequestParam("a") double a, @RequestParam("b") double b) {
        return a * b;
    }


    @RequestMapping("/division")
    public double division(@RequestParam("a") double a, @RequestParam("b") double b) {
        if (b != 0) {
            return a / b;
        }
        else {
            return -9999;
        }
    }

}